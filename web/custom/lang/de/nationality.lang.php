<?php
/* Copyright (C) 2016-2020  Stephan Kreutzer
 *
 * This file is part of note system for refugee-it.de.
 *
 * note system for refugee-it.de is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * note system for refugee-it.de is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with note system for refugee-it.de. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/custom/lang/de/nationality.lang.php
 * @author Stephan Kreutzer
 * @since 2016-11-27
 */



define("LANG_CUSTOM_NATIONALITY_UNKNOWN", "unbekannt");
define("LANG_CUSTOM_NATIONALITY_SYRIAN", "Syrisch");
define("LANG_CUSTOM_NATIONALITY_IRAQI", "Irakisch");
define("LANG_CUSTOM_NATIONALITY_AFGHAN", "Afghanisch");
define("LANG_CUSTOM_NATIONALITY_PAKISTANI", "Pakistanisch");
define("LANG_CUSTOM_NATIONALITY_GAMBIAN", "Gambisch");
define("LANG_CUSTOM_NATIONALITY_KOSOVAR", "Kosovarisch");
define("LANG_CUSTOM_NATIONALITY_ALGERIAN", "Algerisch");
define("LANG_CUSTOM_NATIONALITY_SOMALI", "Somalisch");
define("LANG_CUSTOM_NATIONALITY_ERITREAN", "Eritreisch");
define("LANG_CUSTOM_NATIONALITY_GUINEAN", "Guineisch");
define("LANG_CUSTOM_NATIONALITY_NIGERIAN", "Nigerianisch");
define("LANG_CUSTOM_NATIONALITY_IRANIAN", "Iranisch");
define("LANG_CUSTOM_NATIONALITY_GHANAIAN", "Ghanaisch");
define("LANG_CUSTOM_NATIONALITY_PALESTINIAN", "Palästinensisch");



?>
